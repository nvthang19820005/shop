package com.example.product.config;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import org.springframework.web.multipart.MultipartFile;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.password.Pbkdf2PasswordEncoder;

import java.io.InputStream;
import java.nio.file.*;

@Service
public class Tools {

    @Autowired
    private Environment env;
    private final Path rootLocation;
    Tools() {

        this.rootLocation = Paths.get("src\\main\\resources\\upload");

    }
    public String encryption (String password)
    {
        String passHash=env.getProperty("password.hash");  // secret key used by password encoding
        int iterations = 200000;  // number of hash iteration
        int hashWidth = 256;      // hash width in bits
        Pbkdf2PasswordEncoder pbkdf2PasswordEncoder =
        new Pbkdf2PasswordEncoder(passHash, iterations, hashWidth);
        pbkdf2PasswordEncoder.setEncodeHashAsBase64(true);
        String encodedPassword = pbkdf2PasswordEncoder.encode(password);
        return encodedPassword;
    }
    public String deencryption (String password)
    {
        String passHash=env.getProperty("password.hash");  // secret key used by password encoding
        int iterations = 200000;  // number of hash iteration
        int hashWidth = 256;      // hash width in bits
        Pbkdf2PasswordEncoder pbkdf2PasswordEncoder =
        new Pbkdf2PasswordEncoder(passHash, iterations, hashWidth);
        pbkdf2PasswordEncoder.setEncodeHashAsBase64(true);
        String encodedPassword = pbkdf2PasswordEncoder.encode(password);
        return encodedPassword;
    }
    public String uploadFile(MultipartFile file,String filename) {
        try {
            if (file.isEmpty()) {
                System.out.println("File is nulll");
            }
            
            try (InputStream inputStream = file.getInputStream()) {
               
                Files.copy(file.getInputStream(), this.rootLocation.resolve(filename),
                StandardCopyOption.REPLACE_EXISTING); 
                System.out.println("ok");
                        return "ok";
            }
        } catch (Exception e) {
            System.out.println(e);
            System.out.println("error");
        }
        return null;
    }
}

  
  
  

