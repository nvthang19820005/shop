package com.example.product.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;
import com.example.product.model.BillDetail;
import org.springframework.stereotype.Repository;
@Repository
public interface BillDetailRepository extends JpaRepository<BillDetail, Integer > {
    // public List<BillDetail> findByBill(int id);
    
}
