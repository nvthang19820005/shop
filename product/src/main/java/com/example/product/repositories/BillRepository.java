package com.example.product.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.product.model.Bill;

import org.springframework.stereotype.Repository;
@Repository
public interface BillRepository extends JpaRepository<Bill, Integer >{
    // public List<Bill> findByUser(int id);

    // public List<Bill> findByDate(String date);
}
