package com.example.product.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.product.model.Product;

import org.springframework.stereotype.Repository;
@Repository
public interface ProductRepository extends JpaRepository<Product, Integer>{
    // public List<Product> findByIdCategory(int id);
}
