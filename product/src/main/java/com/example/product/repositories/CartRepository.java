package com.example.product.repositories;
import org.springframework.data.jpa.repository.JpaRepository;

import com.example.product.model.Cart;
import java.util.*;
import org.springframework.stereotype.Repository;
@Repository
public interface CartRepository extends JpaRepository<Cart, Integer >{
    // public List<Cart> findByUser(int id);
}
