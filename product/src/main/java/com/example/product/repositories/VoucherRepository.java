package com.example.product.repositories;
import org.springframework.data.jpa.repository.JpaRepository;

import com.example.product.model.Voucher;
import java.util.List;
import org.springframework.stereotype.Repository;
@Repository
public interface VoucherRepository extends JpaRepository<Voucher, Integer>{
    // public List<Voucher> findByUser(int id);
    
}
