package com.example.product.repositories;

import org.springframework.data.jpa.repository.JpaRepository;


import com.example.product.model.Category;
import org.springframework.stereotype.Repository;
@Repository
public interface CategoryRepository extends JpaRepository<Category, Integer >{

    
}
