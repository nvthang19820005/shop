package com.example.product.repositories;
import org.springframework.data.jpa.repository.JpaRepository;

import com.example.product.model.Coupon;
import org.springframework.stereotype.Repository;
@Repository
public interface CouponRepository extends JpaRepository<Coupon, Integer >{
    
}
