package com.example.product.model;
import javax.persistence.*;
import javax.validation.constraints.*;

import lombok.*;

@Entity
@Getter
@Setter
@Data
@Table(name = "coupon")
public class Coupon {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int idCoupon;

    @NotNull(message = "not be null")
    @Column(length=100)
    String nameCoupon;

    @NotNull(message = "not be null")
    int discount;
    
}
