package com.example.product.model;

import javax.persistence.*;
import javax.validation.constraints.*;

import lombok.*;

@Entity
@Getter
@Setter
@Data
@Table(name = "billdetail")
public class BillDetail {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int idBillDetail;

    @ManyToOne
    @JoinColumn(name = "idBill")
    Bill idBill; 

    @ManyToOne
    @JoinColumn(name = "idProduct")
    Product idProduct;

    @NotNull(message = "not be null")
    int quantity;

    @NotNull(message = "not be null")
    int total;

        
}
