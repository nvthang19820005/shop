package com.example.product.model;
import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.*;


import lombok.*;

@Entity
@Getter
@Setter
@Data
@Table(name = "bill")
public class Bill {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int idBill;

    @ManyToOne
    @JoinColumn(name = "bill")
    Admin idAdmin;

    @ManyToOne
    @JoinColumn(name = "idUser")
    User idUser;

    @NotNull(message = "not be null")
    @Column(length=100)
    String date;

    @NotNull(message = "not be null")
    @Column(length=500)
    String address;

    @NotNull(message = "not be null")
    int idCoupon;

    @NotNull(message = "not be null")
    int total;
    
    @OneToMany(mappedBy = "idBill", cascade = CascadeType.ALL)
    Set<BillDetail> billdetail;
    
    
}
