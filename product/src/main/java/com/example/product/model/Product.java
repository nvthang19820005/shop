package com.example.product.model;

import javax.persistence.*;
import javax.validation.constraints.*;

import com.fasterxml.jackson.annotation.*;

import java.util.*;


import lombok.*;

@Entity
@Getter
@Setter
@Data
@Table(name = "product")
public class Product {  
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int idProduct;

    @ManyToOne
    @JoinColumn(name = "idCategory")
    // @JsonIgnore
    @JsonBackReference
    Category idCategory;

    @NotNull(message = "not be null")
    @Column(length=100)
    String nameProduct; 

    @NotNull(message = "not be null")
    int quantity; 

    @NotNull(message = "not be null")
    int price;

    @NotNull(message = "not be null")
    @Column(length=500)
    String describe;

    @NotNull(message = "not be null")
    @Column(length=500)
    String image;

    @OneToMany(mappedBy = "idBillDetail", cascade = CascadeType.ALL)
    @JsonIgnore
    Set<BillDetail> billdetail;

    @OneToMany(mappedBy = "idCart", cascade = CascadeType.ALL)
    @JsonIgnore
    Set<Cart> cart;    
    
}
