package com.example.product.model;

import javax.persistence.*;
import javax.validation.constraints.*;

import lombok.*;

@Entity
@Getter
@Setter
@Data
@Table(name = "voucher")
public class Voucher {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int idVoucher;

    @NotNull(message = "not be null")
    int idUser;

    @NotNull(message = "not be null")
    int idCoupon;
  
}
