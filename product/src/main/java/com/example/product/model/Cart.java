package com.example.product.model;
import javax.persistence.*;



import lombok.*;

@Entity
@Getter
@Setter
@Data
@Table(name = "cart")
public class Cart {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int idCart;


    @OneToOne
    @JoinColumn(name = "idUser")
    User idUser;

    @ManyToOne
    @JoinColumn(name = "idProduct")
    Product idProduct;

 
}
