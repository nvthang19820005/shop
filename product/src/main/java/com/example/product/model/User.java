package com.example.product.model;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.*;
import lombok.*;

@Entity
@Getter
@Setter
@Data
@Table(name = "user")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int idUser;

    @NotNull(message = "not be null")
    @Column(length=100)
    String nameUser;

    @NotNull(message = "not be null")
    @Column(length=100)
    String email;

    @NotNull(message = "not be null")
    @Column(length=100)
    String phone;

    @NotNull(message = "not be null")
    @Column(length=100)
    String unUser;

    @NotNull(message = "not be null")
    @Column(length=100)
    String pwUser;

    @NotNull(message = "not be null")
    @Column(length=500)
    String image;

    @OneToMany(mappedBy = "idUser", cascade = CascadeType.ALL)
    Set<Bill> bill;

    @Transient
    private String token;


    
    
}
