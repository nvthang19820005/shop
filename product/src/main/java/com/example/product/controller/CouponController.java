package com.example.product.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.example.product.model.Coupon;
import com.example.product.service.CouponService;

@RestController
@RequestMapping(path="/api/coupon")
public class CouponController {
    @Autowired
    CouponService coupon;

    @GetMapping("/getAllCategory")
    public List<Coupon> getAllCoupon() 
    {
        return coupon.getAllCoupon();
    }
}
