package com.example.product.controller;

import java.util.List;
import java.util.UUID;

import org.springframework.http.ResponseEntity;
// import org.springframework.security.authentication.AuthenticationManager;
// import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
// import org.springframework.security.core.Authentication;
// import org.springframework.security.core.context.SecurityContextHolder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import com.example.product.model.User;
import com.example.product.service.UserService;
import com.example.product.jwt.*;
import com.example.product.config.*;

import org.springframework.web.server.ResponseStatusException;

import org.springframework.http.HttpStatus;


@RestController
@RequestMapping(path="/api/user")
public class UserController {
    HttpStatus status;

    @Autowired
    Tools tool;

    @Autowired
    UserService uService;

    @Autowired
    JwtTokenProvider tokenProvider;

    @GetMapping("/getAllUser")
    public List<User> getAllUser() {
        return uService.getAllUser();
    }

    @GetMapping("/user/{id}")
    public User userDetail(@PathVariable("id") Integer  id) {
        
        return uService.getUserDetail(id);
    }

    // @PostMapping("/login")
    // public User login(@RequestBody User user) 
    // {
    //     User u = users.login(user);
    //     if(u!=null)
    //     {  
    //         if(!user.getPwUser().equals(u.getPwUser()))
    //         {
    //             u = null;  
    //         }
    //     }
    //     return u;
    // }

    @PostMapping("/login")
    public User authenticateUser(@RequestBody User  users) {
        users.setPwUser(tool.encryption(users.getPwUser()));
        users=uService.login(users);
       if(users!=null)
       {
        // Trả về jwt cho người dùng.
        String jwt = tokenProvider.generateToken( users);
        users.setToken(jwt);
        return users;
       }
       throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Wrong username or password");
     
        
    }
  
    @PostMapping("/register")
    public ResponseEntity<User> register(@RequestPart("info") User users ,@RequestParam("file") MultipartFile file ) {
 
        try
        {
            String uuid = UUID.randomUUID().toString();
            String filename = "user\\"+uuid+"."+file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".") + 1);
            tool.uploadFile(file, filename);
            users.setImage(filename);
            users.setPwUser(tool.encryption(users.getPwUser()));
            users=  (User) uService.register(users); 
            return new ResponseEntity(users, HttpStatus.CREATED);

        }
        catch(Exception e)
        {
            String error=(String) uService.register(users);
            throw new ResponseStatusException(HttpStatus.CONFLICT,error);   
        }
    }
    @PutMapping("edit/{id}")
    public User edit( @RequestPart("info") User users,
    @PathVariable("id") Integer  id
    ,@RequestParam("file") MultipartFile file)
    {
        if(file==null)
        {
            System.out.println("file null");
        }
        users.setIdUser(id);
        String uuid = UUID.randomUUID().toString();
        String filename = "user\\"+uuid+"."+file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".") + 1);
        tool.uploadFile(file, filename);
        users.setImage(filename);
        uService.edit(users);
        return users;
    }

    @DeleteMapping("delete/{id}")
    public void delete(@PathVariable("id") int id )
    {
        uService.delete(id);
    }


}
