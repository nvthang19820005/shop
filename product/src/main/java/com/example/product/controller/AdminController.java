package com.example.product.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.example.product.model.Admin;
import com.example.product.service.AdminService;

@RestController
@RequestMapping(path="/api/admin")
public class AdminController {
    @Autowired
    AdminService admins;

    @GetMapping("/getAllAdmin")
    public List<Admin> getAllAdmin() {
        return admins.getAllAdmin();
    }

    @GetMapping("/admin/{id}")
    public Admin userDetail(@PathVariable("id") Integer  id) {
        
        return admins.getAdminDetail(id);
    }

    // @PostMapping("/login")
    // public Admin login(@RequestBody Admin admin) 
    // {
    //     Admin u = admins.login(admin);
    //     if(u!=null)
    //     {  
    //         if(!admin.getPwAdmin().equals(u.getPwAdmin()))
    //         {
    //             u = null;
    //         }
    //     }
    //     return u;
    // }

}
