package com.example.product.controller;

import java.util.*;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.example.product.model.Product;

import com.example.product.service.ProductService;

import org.springframework.web.multipart.MultipartFile;

import com.example.product.jwt.*;
import com.example.product.config.*;

@RestController
@RequestMapping(path="api/products")
public class ProductController{
    @Autowired
    Tools tool;

    @Autowired
    ProductService product;

    @GetMapping("/getAllProduct")
    public List<Product> getAllProduct() {
        
        return product.getAllProduct();
    }
    @GetMapping("/product/{id}")
    public Product productDetail(@PathVariable("id") Integer  id) {
        
        return product.getProductDetail(id);
    }

    // @GetMapping("/category/{id}")
    // public List<Product> placeProductByCategory(@PathVariable("id") Integer  id) {
        
    //     return product.getProductByCategory(id);
    // }

    @PostMapping("auth/create")
    public Product CreateProductByFile(@RequestPart("info") Product product ,@RequestParam("file") MultipartFile[] file) {

        String uuid = UUID.randomUUID().toString();
        ArrayList<String> image=new ArrayList<String>();
        for(int i=0;i<file.length;i++)
        {
            String filename = "user\\"+uuid+"."+file[i].getOriginalFilename().substring(file[i].getOriginalFilename().lastIndexOf(".") + 1);
            //sys.uploadFile(file[i],"product/"+filename);
            image.add(filename);
        }
        
        // product.setImage(String.join(",", image));
        // productService.create(product);
        return product;
    }

    @PostMapping("/create")
    public Product productCreate(@RequestPart("info") Product products ,@RequestParam("file") MultipartFile file) {

        String uuid = UUID.randomUUID().toString();
        String filename = "product\\"+uuid+"."+file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".") + 1);
        tool.uploadFile(file, filename);
        products.setImage(filename);
        product.create(products);
        return products;
    }

    @PutMapping("edit/{id}")
    public Product edit( @RequestPart("info") Product products,
    @PathVariable("id") Integer  id
    ,@RequestParam("file") MultipartFile file)
    {
        if(file==null)
        {
            System.out.println("file null");
        }
        products.setIdProduct(id);
        String uuid = UUID.randomUUID().toString();
        String filename = "product\\"+uuid+"."+file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".") + 1);
        tool.uploadFile(file, filename);
        products.setImage(filename);
        product.edit(products);
        return products;
    }

    @DeleteMapping("delete/{id}")
    public void delete(@PathVariable("id") int id )
    {
        product.delete(id);
    }
    


}
