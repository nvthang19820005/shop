package com.example.product.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.product.model.Admin;
import com.example.product.repositories.AdminRepository;

@Service
public class AdminService {
    @Autowired
    AdminRepository rep;

    public List<Admin> getAllAdmin()
    {
      return rep.findAll();
    }

    public Admin getAdminDetail(int id) {
        //TODO-get product detail

        return rep.findById(id).get();
    }
    // public boolean checkUsername(String username)
    // {
    //   return rep.findByUnAdmin(username) == null ;
    // }

    // public Admin login(Admin admin) {

    //     return rep.findByUnAdmin(admin.getUnAdmin());
    // }

    public Admin create(Admin admin) {
        return rep.save(admin);
    }
}
