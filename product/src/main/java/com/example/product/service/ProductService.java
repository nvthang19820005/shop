package com.example.product.service;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.product.model.Product;
import com.example.product.repositories.ProductRepository;

@Service
public class ProductService {
    @Autowired
    ProductRepository rep;

    public List<Product> getAllProduct()
    {
      return rep.findAll();
    }

    public Product getProductDetail(int id) {
        //TODO-get product detail

        return rep.findById(id).get();
    }

    // public List<Product> getProductByCategory(Integer id) {
    //     //TODO-get product by category

    //     return rep.findByIdCategory(id);
    // }

    public Product getDetail(int id)
    {
        return rep.findById(id).get();
    }

    public void create(Product u)
    {
        u=rep.save(u);
    }
    public Product edit(Product u) {
        Optional<Product> existing=rep.findById(u.getIdProduct());

		if(existing.isPresent())
		{
            System.out.println("edit");
			Product old=existing.get();
			old.setIdCategory(u.getIdCategory());
			old.setNameProduct(u.getNameProduct());
			old.setQuantity(u.getQuantity());
			old.setPrice(u.getPrice());
            old.setDescribe(u.getDescribe());
            old.setImage(u.getImage());
            rep.save(old);
			return old;

		}
		else
			return null;
    }

    public void delete(int id) {
        Product a=rep.findById(id).get();
		rep.delete(a);
    }
}
