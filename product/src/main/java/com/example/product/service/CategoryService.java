package com.example.product.service;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.product.model.Category;
import com.example.product.repositories.CategoryRepository;

@Service
public class CategoryService {
    @Autowired
    CategoryRepository rep;

    public List<Category> getAllCategory()
    {
      return rep.findAll();
    }

    public Category getCategoryDetail(int id) {
        //TODO-get product detail

        return rep.findById(id).get();
    }

    // public List<Product> getProductByCategory(Integer id) {
    //     //TODO-get product by category

    //     return rep.findByIdCategory(id);
    // }


    public void create(Category u)
    {
        u=rep.save(u);
    }
    public Category edit(Category u) {
        Optional<Category> existing=rep.findById(u.getIdCategory());

		if(existing.isPresent())
		{
      Category old=existing.get();
			old.setNameCategory(u.getNameCategory());			
      rep.save(old);
			return old;

		}
		else
			return null;
    }

    public void delete(int id) {
    Category a=rep.findById(id).get();
		rep.delete(a);
    }

    
}