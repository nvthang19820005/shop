package com.example.product.service;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.example.product.model.User;
import com.example.product.model.CustomUserDetails;
import com.example.product.repositories.UserRepository;

@Service
public class UserService implements UserDetailsService {
    @Autowired
    UserRepository rep;

    String unExists="Username allready exists";

    public List<User> getAllUser()
    {
      return rep.findAll();
    }

    public User getUserDetail(int id) {
        
 
        return rep.findById(id).get();
    }
    public boolean checkUsername(String username)
    {
      return rep.findByUnUser(username) == null ;
    }

    public User login(User user) {

        return rep.findByUnUser(user.getUnUser());
    }

    public User create(User user) {
        return rep.save(user);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        // TODO Auto-generated method stub
        User user = rep.findByUnUser(username);
        if (user == null) {
            throw new UsernameNotFoundException(username);
        }
        return new CustomUserDetails(user);
    }

    public Object register(User u)
    {
        User un=rep.findByUnUser(u.getUnUser());
        
        if(un==null)
        {
          u=  rep.save(u);
        }
        else return unExists;

        return u;
    }


	public UserDetails loadUserById(String userId) {
		User user = rep.findById(Integer.parseInt(userId)).orElseThrow(
                () -> new UsernameNotFoundException("User not found with id : " + userId)
            );

            return new CustomUserDetails(user);

	}

    public User edit(User u) {
        Optional<User> existing=rep.findById(u.getIdUser());

		if(existing.isPresent())
		{
            System.out.println("edit");
			User old=existing.get();
			old.setNameUser(u.getNameUser());
			old.setEmail(u.getEmail());
			old.setPhone(u.getPhone());
            old.setPwUser(u.getPwUser());
            old.setImage(u.getImage());
            rep.save(old);
			return old;

		}
		else
			return null;
    }

    public void delete(int id) {
        User a=rep.findById(id).get();
		rep.delete(a);
    }




}
