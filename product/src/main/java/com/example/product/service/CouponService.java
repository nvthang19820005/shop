package com.example.product.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.product.model.Coupon;
import com.example.product.repositories.CouponRepository;

@Service
public class CouponService {
    @Autowired
    CouponRepository rep;

    public List<Coupon> getAllCoupon()
    {
      return rep.findAll();
    }
}
