package com.example.products.repositories;
import org.springframework.data.jpa.repository.JpaRepository;

import com.example.products.model.*;
import java.util.*;
import org.springframework.stereotype.Repository;
@Repository
public interface ProductImageRepository extends JpaRepository<ProductImage, Integer > {
    public List<ProductImage> findByIdProduct(Product ca);
}
