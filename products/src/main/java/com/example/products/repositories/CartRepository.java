package com.example.products.repositories;
import org.springframework.data.jpa.repository.JpaRepository;

import com.example.products.model.*;
import java.util.*;
import org.springframework.stereotype.Repository;
@Repository
public interface CartRepository extends JpaRepository<Cart, Integer >{
    public List<Cart> findByIdUser(User user);



}
