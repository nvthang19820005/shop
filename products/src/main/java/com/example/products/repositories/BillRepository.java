package com.example.products.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.products.model.*;

import org.springframework.stereotype.Repository;
@Repository
public interface BillRepository extends JpaRepository<Bill, Integer >{
    public List<Bill> findByIdUser(User user);

    public List<Bill> findByDate(String date);
}
