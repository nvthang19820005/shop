package com.example.products.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.products.model.Admin;
import org.springframework.stereotype.Repository;
@Repository
public interface AdminRepository extends JpaRepository<Admin, Integer > {
    // public Admin findByUnAdmin( String username);
}
