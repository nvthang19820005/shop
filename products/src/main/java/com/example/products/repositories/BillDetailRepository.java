package com.example.products.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;
import com.example.products.model.*;
import org.springframework.stereotype.Repository;
@Repository
public interface BillDetailRepository extends JpaRepository<BillDetail, Integer > {
    public List<BillDetail> findByIdBill(Bill bill);
    
}
