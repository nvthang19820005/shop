package com.example.products.repositories;
import org.springframework.data.jpa.repository.JpaRepository;

import com.example.products.model.Coupon;
import org.springframework.stereotype.Repository;
@Repository
public interface CouponRepository extends JpaRepository<Coupon, Integer >{
    
}
