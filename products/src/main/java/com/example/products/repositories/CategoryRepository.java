package com.example.products.repositories;

import org.springframework.data.jpa.repository.JpaRepository;


import com.example.products.model.Category;
import org.springframework.stereotype.Repository;
@Repository
public interface CategoryRepository extends JpaRepository<Category, Integer >{

    
}
