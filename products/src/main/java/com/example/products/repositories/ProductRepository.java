package com.example.products.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.products.model.*;

import org.springframework.stereotype.Repository;
@Repository
public interface ProductRepository extends JpaRepository<Product, Integer>{
    // public List<Product> findByIdCategory(int id);
    public List<Product[]> findByIdCategory(Category ca);
}
