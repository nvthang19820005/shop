package com.example.products.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.products.model.User;
import org.springframework.stereotype.Repository;
@Repository
public interface UserRepository extends JpaRepository<User, Integer >{
    public User findByUnUser( String username);
    // public Users findByUserIdAndPasswordHash(int id,String pasString);
    // public Users findByEmail(String email);

}

