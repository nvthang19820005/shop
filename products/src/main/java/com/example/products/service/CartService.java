package com.example.products.service;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.products.model.*;
import com.example.products.repositories.*;

@Service
public class CartService {
    @Autowired
    CartRepository rep;

    public List<Cart> getCartByIdUser(User user)
    {
      return rep.findByIdUser(user);
    }

    public String delete(Cart ca) {
        rep.delete(ca);
        return "done";
    }
    public Cart create(Cart u)
    {
        return u=rep.save(u);
    }

 }
