package com.example.products.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.products.model.Coupon;
import com.example.products.repositories.CouponRepository;

@Service
public class CouponService {
    @Autowired
    CouponRepository rep;

    public List<Coupon> getAllCoupon()
    {
      return rep.findAll();
    }
}
