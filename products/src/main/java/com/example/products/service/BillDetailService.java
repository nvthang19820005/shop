package com.example.products.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.products.model.*;
import com.example.products.repositories.BillDetailRepository;

@Service
public class BillDetailService {
    @Autowired
    BillDetailRepository rep;

    public List<BillDetail> getBillDetailByBill(Bill bill)
    {
      return rep.findByIdBill(bill);
    }

    public BillDetail create(BillDetail u)
    {
        return u=rep.save(u);
    }

    
}
