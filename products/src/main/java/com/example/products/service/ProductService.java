package com.example.products.service;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.products.model.Category;
import com.example.products.model.Product;
import com.example.products.repositories.ProductRepository;

@Service
public class ProductService {
    @Autowired
    ProductRepository rep;

    public List<Product> getAllProduct()
    {
      return rep.findAll();
    }

    public Product getProductDetail(int id) {
        //TODO-get product detail

        return rep.findById(id).get();
    }

    public List<Product[]> getProductByCategory(Category ca) {
        //TODO-get product by category

        return rep.findByIdCategory(ca);
    }

    public Product getDetail(int id)
    {
        return rep.findById(id).get();
    }

    public Product create(Product u)
    {
        return u=rep.save(u);
    }
    public Product edit(Product u) {
        // Optional<Product> existing=rep.findById(u.getIdProduct());

		// if(existing.isPresent())
		// {
        //     System.out.println("edit");
		// 	Product old=existing.get();
		// 	old.setIdCategory(u.getIdCategory());
		// 	old.setNameProduct(u.getNameProduct());
		// 	old.setQuantity(u.getQuantity());
		// 	old.setPrice(u.getPrice());
        //     old.setDescribe(u.getDescribe());
        //     old.setImage1(u.getImage1());
        //     rep.save(old);
		// 	return old;

		// }
		// else
		// 	return null;
        return u=rep.save(u);
    }

    public String delete(int id) {
        Optional<Product> existing=rep.findById(id);
        if(existing.isPresent()){
            rep.delete(existing.get());
            return "done";
        }
        else return "no found product id";
        
        
    }
}
