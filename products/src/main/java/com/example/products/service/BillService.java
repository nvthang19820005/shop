package com.example.products.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.products.model.*;
import com.example.products.repositories.BillRepository;

@Service
public class BillService {
    @Autowired
    BillRepository rep;


    public List<Bill> getBillByDate(String date)
    {
      return rep.findByDate(date);
    }
    public List<Bill> getBillByUser(User u)
    {
      return rep.findByIdUser(u);
    }

    public Bill getBillById(int u)
    {
      return rep.findById(u).get();
    }

    public Bill create(Bill u)
    {
        return u=rep.save(u);
    }
}
