package com.example.products.model;
import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.*;
import com.fasterxml.jackson.annotation.*;

import lombok.*;

@Entity
@Getter
@Setter
@Data
@Table(name = "admin")
public class Admin {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int idAdmin;

    @NotNull(message = "not be null")
    @Column(length=100)
    String nameAdmin;

    @NotNull(message = "not be null")
    @Column(length=100)
    String email;
    
    @NotNull(message = "not be null")
    @Column(length=100)
    String phone;
    
    @NotNull(message = "not be null")
    @Column(length=100)
    String unAdmin;
    
    @NotNull(message = "not be null")
    @Column(length=100)
    String pwAdmin;
    
    @NotNull(message = "not be null")
    @Column(length=500)
    String image;

    @OneToMany(mappedBy = "idAdmin", cascade = CascadeType.ALL)
    List<Bill> bill;
}
