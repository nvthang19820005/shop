package com.example.products.model;
import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.*;
import com.fasterxml.jackson.annotation.*;

import lombok.*;

@Entity
@Getter
@Setter
@Data
@Table(name = "category")
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int idCategory;

    @NotNull(message = "not be null")
    @Column(length=100)
    String nameCategory;

    @OneToMany(mappedBy = "idCategory", cascade = CascadeType.ALL)
    List<Product> products;
 
}
