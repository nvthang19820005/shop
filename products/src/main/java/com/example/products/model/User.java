package com.example.products.model;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.*;
import lombok.*;
import com.fasterxml.jackson.annotation.*;

@Entity
@Getter
@Setter
@Data
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int idUser;

    @NotNull(message = "not be null")
    @Column(length=100)
    String nameUser;

    @NotNull(message = "not be null")
    @Column(length=100)
    String email;

    @NotNull(message = "not be null")
    @Column(length=100)
    String phone;

    @NotNull(message = "not be null")
    @Column(length=100)
    String unUser;

    @NotNull(message = "not be null")
    @Column(length=100)
    String pwUser;

    @NotNull(message = "not be null")
    @Column(length=500)
    String image;

    @OneToMany(mappedBy = "idUser", cascade = CascadeType.ALL)
    List<Bill> bill;

    @OneToMany(mappedBy = "idUser", cascade = CascadeType.ALL)
    List<Cart> cart; 

    @OneToMany(mappedBy = "idUser", cascade = CascadeType.ALL)
    List<Voucher> voucher; 

    @Transient
    private String token;


    
    
}
