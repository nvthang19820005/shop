package com.example.products.model;

import javax.persistence.*;
import javax.validation.constraints.*;

import com.fasterxml.jackson.annotation.*;

import java.util.*;


import lombok.*;

@Entity
@Getter
@Setter
@Data
@Table(name = "product")
public class Product {  
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int idProduct;

    @ManyToOne
    // @JsonIgnore
    @JsonIgnore
    Category idCategory;

    @NotNull(message = "not be null")
    @Column(length=100)
    String nameProduct; 

    @NotNull(message = "not be null")
    int quantity; 

    @NotNull(message = "not be null")
    int price;

    @Column(length=500)
    String describe;

    // @Column(length=500)
    // String image1;

    // @Column(length=500)
    // String image2;

    // @Column(length=500)
    // String image3;

    // @Column(length=500)
    // String image4;

    // @Column(length=500)
    // String image5;

    @OneToMany(mappedBy = "idProduct", cascade = CascadeType.ALL)
    @JsonIgnore
    List<BillDetail> billdetail;

    @OneToMany(mappedBy = "idProduct", cascade = CascadeType.ALL)
    @JsonIgnore
    List<Cart> cart;  

    @OneToMany(mappedBy = "idProduct", cascade = CascadeType.ALL)
    List<ProductImage> productimage; 
    
    
    
}
