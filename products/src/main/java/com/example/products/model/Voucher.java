package com.example.products.model;

import javax.persistence.*;
import javax.validation.constraints.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.*;

@Entity
@Getter
@Setter
@Data
@Table(name = "voucher")
public class Voucher {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int idVoucher;

    @ManyToOne
    @JsonIgnore
    User idUser;

    @ManyToOne
    Coupon idCoupon;
  
}
