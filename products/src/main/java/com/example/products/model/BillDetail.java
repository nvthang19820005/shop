package com.example.products.model;

import javax.persistence.*;
import javax.validation.constraints.*;
import com.fasterxml.jackson.annotation.*;
import lombok.*;

@Entity
@Getter
@Setter
@Data
@Table(name = "billdetail")
public class BillDetail {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int idBillDetail;

    @ManyToOne
    @JsonIgnore
    Bill idBill; 

    @ManyToOne
    Product idProduct;

    @NotNull(message = "not be null")
    int quantity;

    @NotNull(message = "not be null")
    int total;

        
}
