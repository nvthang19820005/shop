package com.example.products.model;
import java.util.*;

import javax.persistence.*;
import javax.validation.constraints.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.*;

@Entity
@Getter
@Setter
@Data
@Table(name = "coupon")
public class Coupon {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int idCoupon;

    @NotNull(message = "not be null")
    @Column(length=100)
    String nameCoupon;

    @NotNull(message = "not be null")
    int discount;

    @OneToMany(mappedBy = "idCoupon", cascade = CascadeType.ALL)
    @JsonIgnore
    List<Voucher> vourcher;
    
}
