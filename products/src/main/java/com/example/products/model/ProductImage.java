package com.example.products.model;
import javax.persistence.*;
import com.fasterxml.jackson.annotation.*;


import lombok.*;

@Entity
@Getter
@Setter
@Data
@Table(name = "productimage")
public class ProductImage {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int idImage;

    @ManyToOne
    // @JsonIgnore
    @JsonIgnore
    Product idProduct;

    @Column(length=500)
    String imageLink;
}
