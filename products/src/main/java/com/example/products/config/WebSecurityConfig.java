package com.example.products.config;

import java.util.Arrays;

// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.context.annotation.Bean;
// import org.springframework.security.authentication.AuthenticationManager;
// import org.springframework.security.config.BeanIds;
// import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
// import org.springframework.security.config.annotation.web.builders.HttpSecurity;
// import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
// import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
// import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
// import org.springframework.security.crypto.password.PasswordEncoder;
// import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

// import com.example.product.jwt.JwtAuthenticationFilter;
// import com.example.product.service.UserService;



// @EnableWebSecurity
// public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

//     @Autowired
//     UserService userService;

//     @Bean
//     public JwtAuthenticationFilter jwtAuthenticationFilter() {
//         return new JwtAuthenticationFilter();
//     }

//     @Bean(BeanIds.AUTHENTICATION_MANAGER)
//     @Override
//     public AuthenticationManager authenticationManagerBean() throws Exception {
//         // Get AuthenticationManager bean
//         return super.authenticationManagerBean();
//     }

//     @Bean
//     public PasswordEncoder passwordEncoder() {
//         // Password encoder, để Spring Security sử dụng mã hóa mật khẩu người dùng
//         return new BCryptPasswordEncoder();
//     }

//     @Override
//     protected void configure(AuthenticationManagerBuilder auth)
//             throws Exception {
//         auth.userDetailsService(userService) // Cung cáp userservice cho spring security
//             .passwordEncoder(passwordEncoder()); // cung cấp password encoder
//     }

//     @Override
//     protected void configure(HttpSecurity http) throws Exception {
//         http
//                 .cors() // Ngăn chặn request từ một domain khác
//                     .and()
//                 .authorizeRequests()
//                     .antMatchers("/api/login").permitAll() // Cho phép tất cả mọi người truy cập vào địa chỉ này
//                     .anyRequest().authenticated(); // Tất cả các request khác đều cần phải xác thực mới được truy cập

//         // Thêm một lớp Filter kiểm tra jwt
//         http.addFilterBefore(jwtAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);
//     }
// }

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.SecurityFilterChain;
 
@Configuration
public class WebSecurityConfig {
 
	@Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
     
						http.csrf().disable()
						// dont authenticate this particular request
						.authorizeRequests().
						
						antMatchers("/api/**").permitAll().
						antMatchers("/api/**/auth/**").authenticated().
						// all other requests need to be authenticated
						and();
 
        http.headers().frameOptions();
		return http.build();
    }

	@Bean
    MappingJackson2HttpMessageConverter customizedJacksonMessageConverter() {
        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        converter.setSupportedMediaTypes(
                Arrays.asList(
                        MediaType.APPLICATION_JSON,
                        new MediaType("application", "*+json"),
                        MediaType.APPLICATION_OCTET_STREAM));
        return converter;
    }
 

}
