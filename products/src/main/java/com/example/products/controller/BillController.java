package com.example.products.controller;
import java.util.*;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.example.products.model.*;

import com.example.products.service.*;


@RestController
@RequestMapping(path="api/bill")
public class BillController {
    @Autowired
    BillService bill;

    @Autowired
    UserService user;

    @Autowired
    AdminService admin;

    @Autowired
    CartService cart;

    @GetMapping("/user/{id}")
    public List<Bill> getBillByUser(@PathVariable("id") Integer  id) {
        User aa = new User();
        aa.setIdUser(id);
        return bill.getBillByUser(aa);
    }

    @GetMapping("/date")
    public List<Bill> getBillByDate(@RequestBody String  date) {
        
        return bill.getBillByDate(date);
    }

    @PostMapping("/create")
    public Bill billCreate(@RequestParam("idUser") int iduser,@RequestParam("idAdmin") int idadmin, @RequestPart("info") Bill bills) {
        User users =user.getUserDetail(iduser);
        bills.setIdUser(users);

        Admin admins =admin.getAdminDetail(idadmin);
        bills.setIdAdmin(admins);

        return bill.create(bills);
        // Bill p = 
        // int idb = p.getIdBill();
        
        //     for(int i =0;i<file.length;i++)
        //     {
        //         ProductImage pi = new ProductImage();
        //         pi.setIdProduct(product.getProductDetail(idpro));
        //         pi.setImageLink("product\\"+uuid+"\\"+file[i].getOriginalFilename());
        //         pim.create(pi);
        //         tool.uploadFile(file[i],"product/"+uuid+"/"+file[i].getOriginalFilename());

        //     }
       

        
    }
}
