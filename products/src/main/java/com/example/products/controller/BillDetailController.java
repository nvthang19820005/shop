package com.example.products.controller;
import java.util.*;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.example.products.model.*;

import com.example.products.service.*;


@RestController
@RequestMapping(path="api/billdetail")
public class BillDetailController {
    @Autowired
    BillDetailService billDetail;

    @Autowired
    BillService bill;

    @Autowired
    ProductService product;

    @GetMapping("/bill/{id}")
    public List<BillDetail> getBillDetailrByBill(@PathVariable("id") Integer  id) {
        Bill aa = new Bill();
        aa.setIdBill(id);
        return billDetail.getBillDetailByBill(aa);
    }

    @PostMapping("/create")
    public BillDetail billDetailCreate(@RequestParam("idBill") int idbill,@RequestParam("idProduct") int idPro, @RequestPart("info") BillDetail billDetails) {
        Bill bills =bill.getBillById(idbill);
        billDetails.setIdBill(bills);

        Product pro =product.getProductDetail(idPro);
        billDetails.setIdProduct(pro);
    
        return billDetail.create(billDetails);
    }
}
