package com.example.products.controller;

import java.util.*;

import org.springframework.http.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.server.ResponseStatusException;

import org.springframework.http.*;

import com.example.products.model.Category;
import com.example.products.model.*;

import com.example.products.service.*;
import org.springframework.web.multipart.MultipartFile;

import com.example.products.jwt.*;
import com.example.products.config.*;

@RestController
@RequestMapping(path = "api/products")
public class ProductController {
    @Autowired
    Tools tool;

    @Autowired
    ProductService product;

    @Autowired
    ProductImageService pim;

    @Autowired
    CategoryService categoryService;

    @GetMapping("category/{id}")
    public List<Product[]> a(@PathVariable("id") int id) {
        Category aa = new Category();
        aa.setIdCategory(id);
        return product.getProductByCategory(aa);

    }

    @GetMapping("/getAllProduct")
    public List<Product> getAllProduct() {

        return product.getAllProduct();
    }

    @GetMapping("/product/{id}")
    public Product productDetail(@PathVariable("id") Integer id) {

        return product.getProductDetail(id);
    }

    // @GetMapping("/category/{id}")
    // public List<Product> placeProductByCategory(@PathVariable("id") Integer id) {

    // return product.getProductByCategory(id);
    // }

    @PostMapping("auth/create")
    public Product CreateProductByFile(@RequestPart("info") Product product,
            @RequestParam("file") MultipartFile[] file) {

        String uuid = UUID.randomUUID().toString();
        ArrayList<String> image = new ArrayList<String>();
        for (int i = 0; i < file.length; i++) {
            String filename = "user\\" + uuid + "."
                    + file[i].getOriginalFilename().substring(file[i].getOriginalFilename().lastIndexOf(".") + 1);
            // sys.uploadFile(file[i],"product/"+filename);
            image.add(filename);
        }

        // product.setImage(String.join(",", image));
        // productService.create(product);
        return product;
    }

    @PostMapping("/create")
    public Product productCreate(@RequestParam("idCategory") int idCategory, @RequestPart("info") Product products,
            @RequestParam("file") MultipartFile[] file) {

                System.out.println("Step 1");
        Category cate = categoryService.getCategoryDetail(idCategory);
        products.setIdCategory(cate);
        System.out.println("Step 2");
       
        Product p = product.create(products);
        int idpro = p.getIdProduct();

        System.out.println("Step 3");

        if (file == null) {
            System.out.println("file null");
        } else {
            System.out.println("Step 4");

            for (int i = 0; i < file.length; i++) {
                String uuid = UUID.randomUUID().toString();
                ProductImage pi = new ProductImage();
                pi.setIdProduct(product.getProductDetail(idpro));
                pi.setImageLink("product/" + uuid + "." + file[i]
                        .getOriginalFilename()
                        .substring(
                                file[i]
                                .getOriginalFilename()
                                .lastIndexOf(".") + 1)
                        );
                // pi.setImageLink("product/"+uuid);
                pim.create(pi);
                tool.uploadFile(file[i], "product/" + uuid + "."
                        + file[i].getOriginalFilename().substring(file[i].getOriginalFilename().lastIndexOf(".") + 1));

            }
        }
        // products.setImage1("product\\"+uuid+"\\"+file[0].getOriginalFilename());
        // products.setImage2("product\\"+uuid+"\\"+file[1].getOriginalFilename());
        // products.setImage3("product\\"+uuid+"\\"+file[2].getOriginalFilename());
        // products.setImage4("product\\"+uuid+"\\"+file[3].getOriginalFilename());
        // products.setImage5("product\\"+uuid+"\\"+file[4].getOriginalFilename());

        // String filename =
        // "product\\"+uuid+"."+file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".")
        // + 1);
        // tool.uploadFile(file, filename);
        // products.setImage1(filename);
        System.out.println("Step p");

        return p;
    }

    @PutMapping("edit/{id}")
    public Product edit(@RequestParam("idCategory") int idCategory, @RequestPart("info") Product products,
            @PathVariable("id") Integer id, @RequestParam("file") MultipartFile[] file) {
        Product pro = product.getProductDetail(id);

        String uuid = UUID.randomUUID().toString();
        if (file == null) {
            System.out.println("file null");
        } else {
            List<ProductImage> imp = pim.getImageByProduct(pro);
            for (int i = 0; i < imp.size(); i++) {
                pim.delete(imp.get(i));
            }
            for (int i = 0; i < file.length; i++) {
                ProductImage pi = new ProductImage();
                pi.setIdProduct(pro);
                // pi.setImageLink("product\\"+uuid+"\\"+file[i].getOriginalFilename());
                pi.setImageLink("product/" + uuid + "/" + file[i].getOriginalFilename());
                pim.create(pi);
                tool.uploadFile(file[i], "product/" + uuid + "/" + file[i].getOriginalFilename());

            }
        }
        // if(file==null)
        // {
        // System.out.println("file null");
        // }
        // else{
        // // pro.setImage1("product\\"+uuid+"\\"+file[0].getOriginalFilename());
        // // pro.setImage2("product\\"+uuid+"\\"+file[1].getOriginalFilename());
        // // pro.setImage3("product\\"+uuid+"\\"+file[2].getOriginalFilename());
        // // pro.setImage4("product\\"+uuid+"\\"+file[3].getOriginalFilename());
        // // pro.setImage5("product\\"+uuid+"\\"+file[4].getOriginalFilename());
        // }
        // products.setIdProduct(id);
        // String uuid = UUID.randomUUID().toString();
        // String filename =
        // "product\\"+uuid+"."+file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".")
        // + 1);
        // tool.uploadFile(file, filename);
        // products.setImage1(filename);

        // return product.edit(products);

        // products.setImage1("product\\"+uuid+"\\"+file[0].getOriginalFilename());
        // products.setImage2("product\\"+uuid+"\\"+file[1].getOriginalFilename());
        // products.setImage3("product\\"+uuid+"\\"+file[2].getOriginalFilename());
        // products.setImage4("product\\"+uuid+"\\"+file[3].getOriginalFilename());
        // products.setImage5("product\\"+uuid+"\\"+file[4].getOriginalFilename());
        for (int i = 0; i < file.length; i++) {
            tool.uploadFile(file[i], "product/" + uuid + "/" + file[i].getOriginalFilename());

        }
        Category cate = categoryService.getCategoryDetail(idCategory);

        pro.setIdCategory(cate);
        pro.setNameProduct(products.getNameProduct());
        pro.setQuantity(products.getQuantity());
        pro.setPrice(products.getPrice());
        pro.setDescribe(products.getDescribe());

        return product.edit(pro);

    }

    @DeleteMapping("delete/{id}")
    public ResponseEntity<Product> delete(@PathVariable("id") int id) {
        return new ResponseEntity(product.delete(id), HttpStatus.OK);
    }

}
