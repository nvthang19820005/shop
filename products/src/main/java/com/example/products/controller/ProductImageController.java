package com.example.products.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.example.products.model.*;


import com.example.products.service.ProductImageService;

@RestController
@RequestMapping(path="api/image")
public class ProductImageController {
    @Autowired
    ProductImageService img;

    @GetMapping("product/{id}")
    public List<ProductImage> getImageByProduct(@PathVariable("id") int  id)
    {
        Product aa = new Product();
        aa.setIdProduct(id);
        return img.getImageByProduct(aa);
       
    }

    @GetMapping("imageproduct/{id}")
    public ProductImage getImageById(@PathVariable("id") int  id)
    {
        return img.getImageById(id);
       
    }



}
