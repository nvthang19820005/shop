package com.example.products.controller;
import java.util.*;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.example.products.model.*;

import com.example.products.service.*;


@RestController
@RequestMapping(path="api/cart")
public class CartController {
    @Autowired
    CartService cart;

    @Autowired
    ProductService pro;

    @Autowired
    UserService user;

    @GetMapping("/user/{id}")
    public List<Product> getCartByUser(@PathVariable("id") Integer id) {
        User aa = new User();
        aa.setIdUser(id);
        List<Cart> pr = cart.getCartByIdUser(aa);
        List<Product> prod = new ArrayList();
        for (int i = 0; i < pr.size(); i++) {
            prod.add(pro.getDetail(pr.get(i).getIdProduct().getIdProduct()));
        }
        return prod;
    }

    @DeleteMapping("/user/{id}/delete")
    public String delete(@RequestBody int idP, @PathVariable("id") Integer id) {
        User aa = new User();
        aa.setIdUser(id);
        List<Cart> ca = cart.getCartByIdUser(aa);
        for (int i = 0; i < ca.size(); i++) {
            if(ca.get(i).getIdProduct().getIdProduct() == idP){
                cart.delete(ca.get(i));
                break;
            }
        }
        return "Done";
    }

    @PostMapping("/create")
    public Cart cartCreate(@RequestParam("idUser") int idus, @RequestParam("idProduct") int idProduct) {
        Cart carts = new Cart();
        User us =user.getUserDetail(idus);
        carts.setIdUser(us);
        Product pr =pro.getDetail(idProduct);
        carts.setIdProduct(pr);
        return cart.create(carts);
    }

    @DeleteMapping("/user/{id}/deleteAll")
    public String deleteAll(@PathVariable("id") Integer id) {
        User aa = new User();
        aa.setIdUser(id);
        List<Cart> ca = cart.getCartByIdUser(aa);
        for (int i = 0; i < ca.size(); i++) {
            cart.delete(ca.get(i));
        }
        return "Done";

    }
    
}
