package com.example.products.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.example.products.model.Category;
import com.example.products.service.CategoryService;

@RestController
@RequestMapping(path="/api/category")
public class CategoryController {
    @Autowired
    CategoryService category;

    @GetMapping("/getAllCategory")
    public List<Category> getAllCategory() 
    {
        return category.getAllCategory();
    }
    @GetMapping("/category/{id}")
    public Category categoryDetail(@PathVariable("id") Integer  id) {
        
        return category.getCategoryDetail(id);
    }

    @PostMapping("auth/create")
    public Category CreateProductByFile(@RequestPart("info") Category category ) {

    
        
        // product.setImage(String.join(",", image));
        // productService.create(product);
        return category;
    }

    @PostMapping("/create")
    public Category productCreate(@RequestPart("info")Category categorys ) {
        category.create(categorys);
        return categorys;
    }

    @PutMapping("edit/{id}")
    public Category edit( @RequestBody Category categorys,
    @PathVariable("id") Integer  id)
    {
        categorys.setIdCategory(id);
        System.out.print(categorys.getNameCategory()+"ctrrrrrrrrrrrrrrrrr");
        return category.edit(categorys);
    }

    @DeleteMapping("delete/{id}")
    public void delete(@PathVariable("id") int id )
    {
        category.delete(id);
    }

    // @GetMapping("/category/product/{id}")
    // public Category productDetail(@PathVariable("id") Integer  id) {
        
    //     return category.getCategoryDetail(id);
    // }
    
}
