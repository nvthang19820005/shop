import "./App.css";
import ListProduct from "./product/ListProduct";
import AddProduct from "./product/AddProduct";
import ListCategory from "./category/ListCategory";
import Nav from "./view/Nav";


import React from "react";
import {
  BrowserRouter as Router,
  Routes,
  Switch,
  Route,
  Link
} from "react-router-dom";



function App() {
  return (
    <Router>
    <div className="App">
      <Nav/>
      <Routes>
        <Route path="/" element="Home"/>
        <Route path="/product"  element={<ListProduct/>}/>
        <Route path="/category" element={<ListCategory/>}/>  
        <Route path="/addproduct" element={<AddProduct/>}/> 
      </Routes>
    
    </div>
    </Router>
  );
}

export default App;
