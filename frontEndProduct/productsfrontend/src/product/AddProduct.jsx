import React, { useEffect, useState } from "react";
import { AddProducts } from "../services/Product";

const AddProduct = () => {
  const [nameCate, setNameCate] = useState("");
  const [quantity, setQuantity] = useState("");
  const [price, setPrice] = useState("");
  const [describe, setDescribe] = useState("");

  const [selectedFile, setSelectedFile] = useState();
  const [isFilePicked, setIsFilePicked] = useState(false);

  const handleSubmit = (event) => {
    event.preventDefault();
    
    const item = {
      nameProduct: nameCate,
      quantity: quantity,
      price: price,
      describe: describe,
    };

    AddProducts(item, selectedFile);
  };

  const changeHandler = (event) => {
    setSelectedFile(event.target.files[0]);
    setIsFilePicked(true);
  };

  return (
    <div>
      <form onSubmit={handleSubmit}>
        <label>
          <p>Name Product</p>
          <input
            type="text"
            onChange={(event) => setNameCate(event.target.value)}
            value={nameCate}
          />
          <p>Quantity</p>
          <input
            type="text"
            onChange={(event) => setQuantity(event.target.value)}
            value={quantity}
          />
          <p>Price</p>
          <input
            type="text"
            onChange={(event) => setPrice(event.target.value)}
            value={price}
          />
          <p>Describe</p>
          <input
            type="text"
            onChange={(event) => setDescribe(event.target.value)}
            value={describe}
          />
        </label>

        <input type="file" name="file" onChange={changeHandler} />
        <button type="submit">ADD</button>
      </form>
    </div>
  );
};

export default AddProduct;
