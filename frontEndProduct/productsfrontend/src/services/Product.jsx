import axios from "axios";

const URL_PRODUCT = "http://localhost:8888/api/products";

export function AddProducts(item, selectedFile) {
  const formData = new FormData();
  console.log(selectedFile);
  console.log(item);
  formData.append("file", selectedFile);
  formData.append("idCategory", 1);
  // formData.append("file", new Blob([selectedFile],null),{type:'image/*'});
  formData.append("info", JSON.stringify(item));

  const config = {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  };
  return axios
    .post(URL_PRODUCT + `/create`, formData, config)
    .then((result) => result.data);
}
