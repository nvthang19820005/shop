import { Outlet, Link } from "react-router-dom";
import './Nav.css';

const Layout = () => {
  return (
    <>
      <div className="topnav">
        <a className="active" href="#home">
          <Link to="/">Product</Link>
        </a>
        <a href="category"><Link to="/">Category</Link></a>

      </div>

      <Outlet />
    </>
  );
};

export default Layout;
