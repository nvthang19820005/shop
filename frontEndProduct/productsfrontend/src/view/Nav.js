import "./Nav.css";
import {  NavLink } from "react-router-dom";
const Nav = () => {
  return (
    <div className="topnav">
      <NavLink to="/">
            Home
    </NavLink>
    <NavLink  to="/product">
            Product
    </NavLink>
    <NavLink  to="/category">
            Category
    </NavLink>
    <NavLink  to="/bill">
            Bill
    </NavLink>
    <NavLink  to="/user">
            User
    </NavLink>
    <NavLink  to="/addproduct">
            Add
    </NavLink>
    </div>
  );
};
export default Nav;
