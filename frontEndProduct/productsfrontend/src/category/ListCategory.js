import React, { useEffect, useState } from "react";
import axios from "axios";

const URL_CATEGORY = "http://localhost:8888/api/category";

const ListCategory = () => {
  const [dataCategory, setDataCategory] = useState([]);

  
  useEffect(() => {
    axios.get(URL_CATEGORY + `/getAllCategory`).then((result) => {
      if (result.data) {
        setDataCategory(result.data);
      }
    });
  },[]);
  
  const deleteProduct = (id) =>{
    alert(id);
    axios.delete(URL_CATEGORY+ `/delete/`+id);
    
  }

  return (
    <div
      style={{
        with: "100%",
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <h1>List Category</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Category Id</th>
            <th scope="col">Category Name</th>
            
          </tr>
        </thead>
        <tbody>
          {dataCategory.map((item, index) => {
            return (
              <tr key={item.idCategory}>
                <th scope="row">{index + 1}</th>
                <td>{item.idCategory}</td>
                <td>{item.nameCategory}</td>
                
                <td><i className="fa fa-pencil-square" style={{margin:"5px",fontSize:"30px"}} aria-hidden="true"></i> <i className="fa fa-trash" style={{margin:"5px", fontSize:"30px"}} aria-hidden="true"  onClick={()=>deleteProduct (item.idProduct)} ></i></td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
};

export default ListCategory;
