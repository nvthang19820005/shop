import React, { useEffect, useState } from "react";
import axios from "axios";
import { AllProduct } from "../services/Product";
import { AddToCart } from "../services/Cart";
import "../sass/Home.css";

// const URL_PRODUCT = "http://localhost:8888/api/products";

const Home = () => {
  const [dataProducts, setDataProducts] = useState([]);
  useEffect(() => {
    AllProduct().then((products) => setDataProducts(products));
  }, []);

  const AddCart = (idProduct) => {
    AddToCart(idProduct);
  };

  return (
    <>
      <div className="Card-Home">
        {dataProducts.map((item, index) => (
          <div className="card col-3 " key={index}>
            <img
              src={`http://localhost:8888/` + item.productimage[0].imageLink}
              className="card-img-top"
              alt="..."
            />
            <div className="card-body">
              <h5 className="card-title">{item.nameProduct}</h5>
              <p className="card-text">{item.price.toLocaleString('vi', { style: 'currency', currency: 'VND' })}</p>
              <a href="/"
                className="btn btn-primary"
                onClick={() => AddCart(item.idProduct)}
              >
                Add to cart
              </a>
            </div>
          </div>
        ))}
      </div>
    </>
  );
};

export default Home;
