import React, { useEffect, useState } from "react";
import {CartUser} from "../services/Cart"
const Cart = () => {
  const [dataProducts, setDataProducts] = useState([]);

  useEffect(() => {
    CartUser(10)
    .then((products) =>setDataProducts(products));
  }, []);
  return (
    <div
      style={{
        with: "100%",
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <h1>List Product</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Product Name</th>
            <th scope="col">Quantity</th>
            <th scope="col">Price</th>
            <th scope="col">Describe</th>
            <th scope="col">Product Image</th> 
          </tr>
        </thead>
        <tbody>
          {dataProducts.map((item, index) => {
            return (
              <tr key={item.idProduct}>
                <th scope="row">{index + 1}</th>
                <td>{item.nameProduct}</td>
                <td>{item.quantity}</td>
                <td>{item.price}</td>
                <td>{item.describe}</td>
                <td>
                    {
                        item.productimage.map((i,a)=>{
                           return (<img 
                            key={a}
                            style={{padding:"3px"}}
                            width="50"
                            height="50"
                            src={`http://localhost:8888/${i.imageLink}`}
                            alt="new"
                          />)
                        })
                    }

                </td>
                <td><i className="fa fa-trash" style={{margin:"5px", fontSize:"30px"}} aria-hidden="true" ></i></td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default Cart