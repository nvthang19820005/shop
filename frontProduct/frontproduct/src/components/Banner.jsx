import React from "react";

import "../sass/Banner.css";
import Anh1 from "../img/banner1.jpeg";


const Banner = () => {
  return (
    //       <div id="carouselExampleControls" className="carousel slide" data-bs-ride="carousel">
    //   <div className="carousel-inner">
    //     {/* <div className="carousel-item active">
    //       <img src={Anh1} className="d-block w-100" alt="..."/>
    //     </div> */}
    //     <div className="carousel-item">
    //       <img src={Anh2} className="d-block w-100" alt="deo co"/>
    //     </div>
    //     <div className="carousel-item">
    //       <img src={Anh3} className="d-block w-100" alt="..."/>
    //     </div>
    //   </div>
    //   <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
    //     <span className="carousel-control-prev-icon" aria-hidden="true"></span>
    //     <span className="visually-hidden">Previous</span>
    //   </button>
    //   <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
    //     <span className="carousel-control-next-icon" aria-hidden="true"></span>
    //     <span className="visually-hidden">Next</span>
    //   </button>
    // </div>

    <div
      id="carouselExampleControls"
      className="carousel slide"
      data-bs-ride="carousel"
    >
      <div className="carousel-inner">
        <div className="carousel-item active">
          <img src={Anh1} className="d-block w-100" alt="..." />
        </div>
        <div className="carousel-item">
          <img src={Anh1} className="d-block w-100" alt="..." />
        </div>
        <div className="carousel-item">
          <img src={Anh1} className="d-block w-100" alt="..." />
        </div>
      </div>
      <button
        className="carousel-control-prev"
        type="button"
        data-bs-target="#carouselExampleControls"
        data-bs-slide="prev"
      >
        <span className="carousel-control-prev-icon" aria-hidden="true"></span>
        <span className="visually-hidden">Previous</span>
      </button>
      <button
        className="carousel-control-next"
        type="button"
        data-bs-target="#carouselExampleControls"
        data-bs-slide="next"
      >
        <span className="carousel-control-next-icon" aria-hidden="true"></span>
        <span className="visually-hidden">Next</span>
      </button>
    </div>
  );
};

export default Banner;
