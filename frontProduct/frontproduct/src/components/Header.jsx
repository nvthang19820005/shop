import React from "react";
import Nav from "./Nav";
import Banner from "./Banner";

import Home from "../pages/Home";
import Cart from "../pages/Cart";
// import Routers from '../router/Routers'
// import { Router, Routes, Route } from "react-router-dom";

import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

// import Home from "../pages/Home";
const Header = () => {
  return (
    <Router>
      <div>
        <Nav />
        <Banner/>
        <Routes>

          <Route path="/" element={<Home/>}  />
          <Route path="/product" element="Product" />
          <Route path="/category" element="Category" />
          <Route path="/cart" element={<Cart/>} />
        </Routes>
      </div>
    </Router>
  );
};

export default Header;
