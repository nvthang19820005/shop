import React from 'react'

import {
    BrowserRouter as
    Routes,
    Route
  } from "react-router-dom";
  

const Routers = () => {
  return (
      <Routes>
        <Route path="/" element="Home"/>
        <Route path="/product"  element="Product"/>
        <Route path="/category" element="Category"/>  
      </Routes>
  )
}

export default Routers
