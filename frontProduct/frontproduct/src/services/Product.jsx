import axios from "axios";

const URL_PRODUCT = "http://localhost:8888/api/products";

export function AllProduct () {
  return  axios.get(URL_PRODUCT + `/getAllProduct`).then(result => result.data); 
}

