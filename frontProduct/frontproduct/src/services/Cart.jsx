import axios from "axios";

const URL_PRODUCT = "http://localhost:8888/api/cart";

export function AddToCart(item) {
  const formData = new FormData();
  formData.append("idUser", 10);
  formData.append("idProduct", item);
  
  console.log(formData);

  return axios
    .post(URL_PRODUCT + `/create`, formData)
    .then((result) => result.data);
}
export function CartUser(idUser) {
  return axios
    .get(URL_PRODUCT + `/user/` + idUser)
    .then((result) => result.data);
}
